# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class Noticias(scrapy.Item):
    cidade_regiao = scrapy.Field()
    url = scrapy.Field()
    site = scrapy.Field()
    contexto = scrapy.Field()
    titulo = scrapy.Field()
    subtitulo = scrapy.Field()
    conteudo = scrapy.Field()
    data_publicacao = scrapy.Field()
    data_atualizacao = scrapy.Field()
    criadopor = scrapy.Field()

class Incra(scrapy.Item):
    sr = scrapy.Field()
    codigo_imovel = scrapy.Field()
    no_certificacao = scrapy.Field()
    data_certificacao = scrapy.Field()
    no_processo = scrapy.Field()
    nome_imovel = scrapy.Field()
    area_peca_tecnica_ha = scrapy.Field()
