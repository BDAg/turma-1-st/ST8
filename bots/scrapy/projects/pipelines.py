# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import pymongo
from scrapy.conf import settings
from scrapy.exceptions import DropItem
import logging

class MongoDBPipeline(object):

    def __init__(self):
        # cria a conexão com o banco de dados
        #connection = pymongo.MongoClient('mongodb://lucas:bigdata@cluster1-shard-00-00-cugxr.mongodb.net:27017,cluster1-shard-00-01-cugxr.mongodb.net:27017,cluster1-shard-00-02-cugxr.mongodb.net:27017/st8db?ssl=true&replicaSet=cluster1-shard-0&authSource=admin')
        connection = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        self.db = connection[settings['MONGODB_DB']]

    def process_item(self, item, spider):

        collection = self.db[type(item).__name__.lower()]

        valid = True
        for data in item:
            # valida se o registro é válido
            if not data:
                valid = False
                raise DropItem("Faltando item: {0}!".format(data))
        if valid:
            # insere registro no banco de dados
            logging.info(collection.update(item, dict(item), check_keys=False, upsert=True))
        return item
