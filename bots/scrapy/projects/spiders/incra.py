#-*- coding: utf-8 -*-
# encoding: iso-8859-1
# encoding: win-1252
import scrapy
import pymongo
from scrapy.conf import settings
from datetime import datetime
from projects.items import Incra

class NaInvasaoSpider(scrapy.Spider):
    name = 'incra'
    nome_fazenda = ''

    def start_requests(self):
        urls = [
                    'http://certificacao.incra.gov.br/Certifica/certREPR_GRAFICA_IMOVELlist.asp?x_UF_MUNICIPIO=PR&z_UF_MUNICIPIO=LIKE&x_COD_MUNICIPIO=4113700&z_COD_MUNICIPIO=LIKE'
        ]

        conexao = pymongo.MongoClient(
            settings['MONGODB_SERVER'],
            settings['MONGODB_PORT']
        )
        db = conexao[settings['MONGODB_DB']]

        # Busca os Parâmetros do Sistema
        parametros = db.parametros.find_one()

        global nome_fazenda
        nome_fazenda = parametros["nome_fazenda"]

        # Loop à partir dos links principais
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):

        registros = response.xpath('//*[@id="fREPR_GRAFICA_IMOVELlist"]/table/tbody/tr')

        for registro in registros:

            #fREPR_GRAFICA_IMOVELlist > table > tbody > tr:nth-child(16) > td:nth-child(1) > div
            sr = registro.xpath('./td/div/text()').extract()[0]
            codigo_imovel = registro.xpath('./td/div/text()').extract()[1]
            no_certificacao = registro.xpath('./td/div/text()').extract()[2]
            str_data_certificacao = registro.xpath('./td/div/text()').extract()[3]
            data_certificacao = datetime.combine(datetime.strptime(str_data_certificacao, '%m/%d/%Y').date(), datetime.min.time())
            no_processo = registro.xpath('./td/div/text()').extract()[4]
            nome_imovel = registro.xpath('./td/div/text()').extract()[5]
            area_peca_tecnica_ha = registro.xpath('./td/div/text()').extract()[6]

            # Tem que usar um objeto de item.py para que seja possível enviar para o banco lá em pipelines.py
            item = Incra()
            item['sr'] = sr.encode('utf-8').strip()
            item['codigo_imovel'] = codigo_imovel.encode('utf-8').strip()
            item['no_certificacao'] = no_certificacao.encode('utf-8').strip()
            item['data_certificacao'] = data_certificacao
            item['no_processo'] = no_processo.encode('utf-8').strip()
            item['nome_imovel'] = nome_imovel.encode('utf-8').strip()
            item['area_peca_tecnica_ha'] = area_peca_tecnica_ha.encode('utf-8').strip()

            yield item

        # Segue para a próxima página, na busca
        next_page = response.xpath('//*[@id="ewpagerform"]/table/tr/td[1]/table/tr/td[5]/a/@href').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)
