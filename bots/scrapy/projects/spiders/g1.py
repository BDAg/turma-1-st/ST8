#-*- coding: utf-8 -*-
# encoding: iso-8859-1
# encoding: win-1252
import scrapy
import pymongo
from scrapy.conf import settings
from datetime import datetime
from projects.items import Incra

class NaInvasaoSpider(scrapy.Spider):
    name = 'g1'
    nome_fazenda = ''

    def start_requests(self):
        urls = [
                    'http://g1.globo.com/minas-gerais/triangulo-mineiro/noticia/grupo-rouba-mais-de-300-cabecas-de-gado-de-fazenda-em-conceicao-das-alagoas.ghtml'
        ]

        # Loop à partir dos links principais
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):

        yield {'subtitulo' : response.xpath('//*[@class="content-head__subtitle"]/text()').extract_first()}
